;
; UNIVERSITY OF COLORADO AT BOULDER
;
; Assembly Program to generate morse code for user given any 5 alphabets
; Submitted during LAB2 of ECEN5613 course
; Written by Kiran Narayana Hegde (kihe6592@colorado.edu)
; Date : 10/05/2017
; Tools used: EdSim51
; Assumptions made: one unit of time = 0.2s		 
;		    According to Internation morse code
;		    time for dot is 1 unit = 0.2s 
;		    time dash is is 3 units = 0.6s 
;		    time between dot and dash is 0.2s
;		    time between alphabets is 0.6s

;--------------------------------------------------------------------------------------------------------------------

	ORG 0000H		
	LJMP BEGIN

	ORG 000BH
	LJMP ISR_TIMER0
	

	ORG 0021H
	CLR P1.1
BEGIN:  MOV 25H, #48H			; input ascii value of 5 alphabets from A to Z
	MOV 26H, #45H			; Here I have taken 'H' 'E' 'G' 'D' 'E'
	MOV 27H, #47H
	MOV 28H, #44H
	MOV 29H, #45H
	

;----------------------------------------------------------------------------------------------------------------------
; Here I have hardcoded the values for every alphabets
; The function performs compare and jump instruction for every alphabet
; I have taken timer0 to generate a delay of 50ms.
; I am looping 50ms delay to generate 0.2s and 0.6s
; For 0.2s, timer0 should be looped 4 times and for 0.6s its is 0C times
; I have put the values accordingly into memory locations starting from 30H
; For ex: Alphabet 'H' has 4 dots, so memory locations 30h-33h contains value 04H in it. 
; Memory locations starting from 69H contains length of morse code 
; For ex: Morse code of E is dot. 70H contains 01H. Morse code of G is dash dash dot. 71H contains 03H
; I have looped 5 times for 5 alphabets
; R0 represents memory 69H
; R1 represents memory location 30H
	


	OV A, 25H
	MOV R1, #30H
	MOV R3, #05H
	MOV R0, #69H
	CJNE R3, #05H, LOAD1
	MOV A, 25H
	SJMP LOOP
LOAD1:  CJNE R3, #04H, LOAD2
	MOV A, 26H
	SJMP LOOP
LOAD2:  CJNE R3, #03H, LOAD3
	MOV A, 27H
	SJMP LOOP
LOAD3:  CJNE R3, #02H, LOAD4
	MOV A, 28H
	SJMP LOOP
LOAD4:  CJNE R3, #01H, HERE22
	MOV A, 29H
	SJMP LOOP
HERE22: LJMP HERE21
LOOP:	MOV R2, A
ALPHAA:	CJNE R2, #41H, ALPHAB			; Compare the ascii value with A
	MOV @R1, #04H				; if true put respective value into memory location
	INC R1					; do this for every alphabet
	MOV @R1, #0CH
	MOV @R0, #02H	
	LJMP NOPE3
ALPHAB: CJNE R2, #42H, ALPHAC
	MOV @R1, #0CH
	INC R1
	MOV @R1, #04H
	INC R1
	MOV @R1, #04H
	INC R1
	MOV @R1, #04H
	MOV @R0, #04H
	LJMP NOPE3
ALPHAC: CJNE R2, #43H, ALPHAD
	MOV @R1, #0CH
	INC R1
	MOV @R1, #04H
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #04H
	MOV @R0, #04H
	LJMP NOPE3
ALPHAD: CJNE R2, #44H, ALPHAE
	MOV @R1, #0CH
	INC R1
	MOV @R1, #04H
	INC R1
	MOV @R1, #04H
	MOV @R0, #03H
	LJMP NOPE3
ALPHAE: CJNE R2, #45H, ALPHAF
	MOV @R1, #04H
	MOV @R0, #01H
	LJMP NOPE3
ALPHAF: CJNE R2, #46H, ALPHAG
	MOV @R1, #04H
	INC R1
	MOV @R1, #04H
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #04H
	MOV @R0, #04H
	LJMP NOPE3
ALPHAG: CJNE R2, #47H, ALPHAH
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #04H
	MOV @R0, #03H
	LJMP NOPE3
ALPHAH:	CJNE R2, #48H, ALPHAI
	MOV @R1, #04H
	INC R1
	MOV @R1, #04H
	INC R1
	MOV @R1, #04H
	INC R1
	MOV @R1, #04H
	MOV @R0, #04H
	LJMP NOPE3
ALPHAI: CJNE R2, #49H, ALPHAJ
	MOV @R1, #04H
	INC R1
	MOV @R1, #04H
	MOV @R0, #02H
	LJMP NOPE3
ALPHAJ: CJNE R2, #4AH, ALPHAK
	MOV @R1, #04H
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	MOV @R0, #04H
	LJMP NOPE3
ALPHAK: CJNE R2, #4BH, ALPHAL
	MOV @R1, #0CH
	INC R1
	MOV @R1, #04H
	INC R1
	MOV @R1, #0CH
	MOV @R0, #03H
	LJMP NOPE3
ALPHAL: CJNE R2, #4CH, ALPHAM
	MOV @R1, #04H
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #04H
	INC R1
	MOV @R1, #04H
	MOV @R0, #04H
	LJMP NOPE3
ALPHAM: CJNE R2, #4DH, ALPHAN
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	MOV @R0, #02H
	LJMP NOPE3
ALPHAN: CJNE R2, #4EH, ALPHAO
	MOV @R1, #0CH
	INC R1
	MOV @R1, #4H
	MOV @R0, #02H
	LJMP NOPE3
ALPHAO: CJNE R2, #4FH, ALPHAP
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	MOV @R0, #03H
	LJMP NOPE3
ALPHAP: CJNE R2, #50H, ALPHAQ
	MOV @R1, #4H
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #4H
	MOV @R0, #04H
	LJMP NOPE3
ALPHAQ: CJNE R2, #51H, ALPHAR
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #4H
	INC R1
	MOV @R1, #0CH
	MOV @R0, #04H
	LJMP NOPE3
ALPHAR: CJNE R2, #52H, ALPHAS
	MOV @R1, #4H
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #4H
	MOV @R0, #03H
	LJMP NOPE3
ALPHAS: CJNE R2, #53H, ALPHAT
	MOV @R1, #4H
	INC R1
	MOV @R1, #4H
	INC R1
	MOV @R1, #4H
	MOV @R0, #03H
	LJMP NOPE3
ALPHAT:	CJNE R2, #54H, ALPHAU
	MOV @R1, #0CH
	MOV @R0, #01H
	LJMP NOPE3
ALPHAU: CJNE R2, #55H, ALPHAV
	MOV @R1, #4H
	INC R1
	MOV @R1, #4H
	INC R1
	MOV @R1, #0CH
	MOV @R0, #03H
	LJMP NOPE3
ALPHAV:	CJNE R2, #56H, ALPHAW
	MOV @R1, #4H
	INC R1
	MOV @R1, #4H
	INC R1
	MOV @R1, #4H
	INC R1
	MOV @R1, #0CH
	MOV @R0, #04H
	LJMP NOPE3
ALPHAW: CJNE R2, #57H, ALPHAX
	MOV @R1, #4H
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	MOV @R0, #03H
	LJMP NOPE3
ALPHAX: CJNE R2, #58H, ALPHAY
	MOV @R1, #0CH
	INC R1
	MOV @R1, #4H
	INC R1
	MOV @R1, #4H
	INC R1
	MOV @R1, #0CH
	MOV @R0, #04H
	LJMP NOPE3
ALPHAY: CJNE R2, #59H, ALPHAZ
	MOV @R1, #0CH
	INC R1
	MOV @R1, #4H
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	MOV @R0, #04H
	LJMP NOPE3
ALPHAZ: CJNE R2, #5AH, NOPE3
	MOV @R1, #0CH
	INC R1
	MOV @R1, #0CH
	INC R1
	MOV @R1, #4H
	INC R1
	MOV @R1, #4H
	MOV @R0, #04H
NOPE3:  NOP
	DJNZ R3, LOOP1
	LJMP HERE21
LOOP1:	INC R0
	INC R1
	LJMP LOAD1
; comparing with alphabets completed
; ----------------------------------------------------------------------------------------------------------------------



;-----------------------------------------------------------------------------------------------------------------------
; blink led functions here

HERE21: NOP
	MOV 64H, #08H
	MOV 65H, #05H
	MOV IE, #82H			; timer initialization
	mov tmod, #01h
	MOV TH0, #4CH
	MOV TL0, #08H
	mov R0, #30h
	mov r1, #69h

LED:	MOV R3, #04H
	SETB P1.1
DELAY1:	SETB TR0
TI_LOOP: JNB TF0, TI_LOOP		; LED is on here
	NOP
	DJNZ 30h, DELAY1 		; generate 0.2s or 0.6s according to 30H location
	
	CLR P1.1
DELAY2:	SETB TR0
TI_LOOP2: JNB TF0, TI_LOOP2		; generate 0.2s, no matter what
	NOP				; LED is off
	DJNZ R3, DELAY2			
	inc r0
	mov 30h, @r0
	DJNZ 69h, LED			; loop until one alphabet morse code is done
	inc r1				; if done go for next alphabet
	mov 69h, @r1
DELAY3: SETB TR0
TI_LOOP3: JNB TF0, TI_LOOP3		; generate extra 0.4s to give a delay of 0.6s between alphabets
	NOP				; 0.6=0.2 from previous loop + 0.4 now 
	DJNZ 64H, DELAY3
	mov 64h, #08h
	djnz 65h, LED			; 65H is for number of alphabets which is fixed for 5
here_loop: CLR P1.1 
infinite: sjmp infinite

ISR_TIMER0:
	CLR TF0
	CLR TR0
	MOV TH0, #4CH
	MOV TL0, #08H
	RETI
;---------------------------------------------------------------------------------------------------------------------------
END

