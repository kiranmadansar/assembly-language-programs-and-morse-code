;
; UNIVERSITY OF COLORADO AT BOULDER
;
; Assembly Program for ECEN 5613 Lab2 Supplemental Element
; Written by Kiran Narayana Hegde (kihe6592@colorado.edu)
; date : 10/05/2017
; In order to store the mentioned values I have taken two address.
; 0025H for main loop and 0048H for ISR value storing
;--------------------------------------------------------------------------------------------------------------------

	.ORG 0000H
	LJMP BEGIN

	.ORG 000BH
	LJMP ISR_TIMER0

BEGIN:  MOV TH0, #4Ch			; Move TH0 and TL0 with values for generating 
	MOV TL0, #08H			; 50 ms of delay
	MOV IE, #82H			; Enable timer0 interrupt
	SETB TR0			; start timer
	MOV R2, #80H			; 80H-FFH value to be stored during ISR 
	MOV DPH, #00H			; Move DPH and DPL values
	MOV DPL, #25H
LOOP:   MOV A, #00H
NEXT:	MOVX @DPTR, A			; 00H to 7FH values to be stored during main loop
	NOP
	INC A				; Increment 
	CJNE A, #80H, NEXT		; Compare if it is equal to 80h, if yes, start from 00H
	SJMP LOOP

ISR_TIMER0:
	cpl p1.1
	CLR TF0
	CLR TR0
	MOV TH0, #4CH
	MOV TL0, #08H			;once the interrupt has occured, initialise the registers again since it is not sutoload mode
	SETB TR0
	MOV R5, A
	MOV A, R2			; move value from 80H-FFH in ISR
	MOV DPH, #00H			; to the location 048H
	MOV DPL, #48H			
	MOVX @DPTR, A
	INC R2
	CJNE R2, #00H, HERE		; If it exceeds FFH, reset the register value to 80H
	MOV R2, #80H
HERE:
	MOV DPH, #00H			; on the safer side, fill DPTR and A with main loop values
	MOV DPL, #25H
	MOV A, R5
	RETI				; return from interrupt
.END
	
