	.ORG 0000h
LJMP BEGIN

	.ORG 000BH		; address for timer0 interrupt
LJMP ISR_TIMER0

BEGIN:  
	CLR P1.7	; CLEAR ISR TOGGLING BIT
	CLR P1.1	; CLEAR LED BIT
	MOV TMOD, #01H	; CONFIGURE TIMER0 FOR MODE1, 16 BIT COUNTER
	MOV IE, #82H	; ENABLE INTERRUPT FOR TIMER0
	MOV TH0, #4CH   ; SET THE NUMBERS IN TH0 & TL0 FOR 50ms DELAY GENERATION 
	MOV TL0, #08H
	SETB TR0	; START TIMER
	MOV R1, #07H	; LOOPING VALUE TO GENERATE 0.4S DELAY
	MOV a, #20H
HERE1:   SJMP HERE1	; INFINITE LOOP

ISR_TIMER0: 
	CPL p1.7	; COMPLEMENT ISR TOGGLING BIT
	CLR TR0		; STOP TIMER0
	CLR TF0
	CJNE R1, #00h, RETURN ; CHECK IF  THE 0.4S DELAY IS DONE
	CPL p1.1	; IF YES, COMPLEMENT LED BIT
	MOV R5, A 
	MOV DPH, #08H
	MOV DPL, #25H
	MOVX A, @DPTR
	INC A
	MOVX @DPTR, A
	CJNE A, #0FFH, HERE
	MOV A, #80H
	MOVX @DPTR, A
HERE:   MOV DPTR, #0FFFH
	MOV A, R5
	MOV R1, #08h	
RETURN:	DEC R1		; LOOPING 8 TIMES TO GENERATE 0.4S DELAY
	MOV TH0, #4ch	; MOVING THE TH0 & TL0 VALUE AGAIN, SINCE IT IS NOT AUTORELOAD MODE
	MOV TL0, #08h 
	SETB TR0	; START TIMER0
	CPL P1.7	
	RETI    	; RETUEN FROM INTERRUPT
.END
